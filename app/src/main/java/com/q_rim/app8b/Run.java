package com.q_rim.app8b;

import java.util.Date;

public class Run {
  private Date startDate;

  public Run() {
    this.startDate = new Date();
  }

  public Date getStartDate() {
    return startDate;
  }

  public void setStartDate(Date startDate) {
    this.startDate = startDate;
  }

  public int getDurationSeconds(long endMilliSec) {
    return (int)((endMilliSec - startDate.getTime()) / 1000);
  }

  public static String formatDuration(int durationSeconds) {
    int seconds = durationSeconds % 60;
    int minutes = ((durationSeconds - seconds) / 60) % 60;
    int hours = (durationSeconds - (minutes * 60) - seconds) / 3600;
    return String.format("%02d:%02d:%02d", hours, minutes, seconds);
  }
}
