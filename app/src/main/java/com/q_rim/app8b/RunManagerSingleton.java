package com.q_rim.app8b;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.location.Location;
import android.location.LocationManager;

public class RunManagerSingleton {
  private static final String TAG = "RunManagerSingleton";
  public static final String ACTION_LOCATION = "com.q_rim.app8b.ACTION_LOCATION";

  private static RunManagerSingleton runManager;
  private Context appContext;
  private LocationManager locationManager;

  // private constructor forces users to use RunManagerSingleton.get(Context)
  private RunManagerSingleton(Context appContext) {
    this.appContext = appContext;
    this.locationManager = (LocationManager) this.appContext.getSystemService(Context.LOCATION_SERVICE);
  }

  public static RunManagerSingleton get(Context c) {
    if (runManager == null) {
      // Use the application context to avoid leaking activities
      runManager = new RunManagerSingleton(c.getApplicationContext());
    }
    return runManager;
  }

  private PendingIntent getLocationPendingIntent(boolean shouldCreate) {
    Intent broadcast = new Intent(ACTION_LOCATION);
    int flags = shouldCreate ? 0 : PendingIntent.FLAG_NO_CREATE;
    return PendingIntent.getBroadcast(appContext, 0, broadcast, flags);
  }

  // API: start location updates - Note that it's a public method.
  public void startLocationUpdates() {
    String provider = LocationManager.GPS_PROVIDER;

    // Last known location - get this first if we have one.
    Location lastknown = this.locationManager.getLastKnownLocation(provider);
    if (lastknown != null) {
      // Reset the time to now - this may or may not be what you want to do for your app.
      lastknown.setTime(System.currentTimeMillis());
      broadcastLocation(lastknown);
    }


    // Start updates from the location manager
    PendingIntent pi = getLocationPendingIntent(true);
    // frequency of GPS location update - set to most frequent.
    // for apps needing less continuous accuracy, this should be turned down.
    this.locationManager.requestLocationUpdates(provider, 0, 0, pi);  
  }

  // API: stop location update
  public void stopLocationUpdates() {
    // creates an Intent to be broadcast when location updates happen.
    PendingIntent pi = getLocationPendingIntent(false);
    if (pi != null) {
      this.locationManager.removeUpdates(pi);
      pi.cancel();
    }
  }

  // API: tell if you're currently tracking a run.
  public boolean isTrackingRun() {
    // null-checking the result - determines whether the PendingIntent is registered with OS.
    return getLocationPendingIntent(false) != null;
  }

  // used for getting the GPS Last Known Location to speed up the location.
  private void broadcastLocation(Location location) {
    Intent broadcast = new Intent(ACTION_LOCATION);
    broadcast.putExtra(LocationManager.KEY_LOCATION_CHANGED, location);
    this.appContext.sendBroadcast(broadcast);
  }
}
